package org.adblockplus.adblockplussbrowser.app.data.prefs

import org.adblockplus.adblockplussbrowser.base.data.prefs.ActivationPreferences
import org.adblockplus.adblockplussbrowser.onboarding.data.prefs.OnboardingPreferences

internal interface AppPreferences: OnboardingPreferences, ActivationPreferences