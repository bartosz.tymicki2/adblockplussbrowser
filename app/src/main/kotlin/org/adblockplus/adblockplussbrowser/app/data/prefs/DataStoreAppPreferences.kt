package org.adblockplus.adblockplussbrowser.app.data.prefs

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

internal class DataStoreAppPreferences(private val dataStore: DataStore<Preferences>) : AppPreferences {

    companion object {
        const val PREFS_NAME = "abp_app_prefs"
    }

    private object Keys {
        val ONBOARDING_COMPLETED = booleanPreferencesKey("onboarding_completed")
        val ACTIVATED = booleanPreferencesKey("activated")
    }

    override val onboardingCompleted: Flow<Boolean> =
        dataStore.data.map { it[Keys.ONBOARDING_COMPLETED] ?: false }

    override suspend fun completeOnboarding() {
        dataStore.edit { preferences ->
            preferences[Keys.ONBOARDING_COMPLETED] = true
        }
    }

    override val activated: Flow<Boolean> =
        dataStore.data.map { it[Keys.ACTIVATED] ?: false }

    override suspend fun activate() {
        dataStore.edit { preferences ->
            preferences[Keys.ACTIVATED] = true
        }
    }
}