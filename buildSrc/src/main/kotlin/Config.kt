object Config {
    const val VERSION_CODE = 19
    const val VERSION_NAME = "2.0.0"

    const val COMPILE_SDK_VERSION = 30
    const val BUILD_TOOLS_VERSION = "30.0.3"
    const val MIN_SDK_VERSION = 21
    const val TARGET_SDK_VERSION = 30

    const val ANDROID_TEST_INSTRUMENTATION_RUNNER = "androidx.test.runner.AndroidJUnitRunner"
}